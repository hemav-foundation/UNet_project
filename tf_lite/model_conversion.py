"""
Created on 18/11/2021

This file is intended to be used as the official Hemav Foundation conversor for ML models. 

The script has to read the vegetation detection models created using keras and tensorflow to a 
tensorflow lite model to be used on Raspberry Pi using 32 bits processor.
"""

import tensorflow as tf
#from tensorflow.contrib import lite
import os

current_path = os.getcwd()
print(current_path)

saved_model = current_path + '/tf_lite/vegetation_model.hdf5'
print(saved_model)


# Convert the model
#converter = tf.lite.TFLiteConverter.from_saved_model(saved_model) # path to the SavedModel directory

model = tf.keras.models.load_model(saved_model, compile=False)

converter = tf.lite.TFLiteConverter.from_keras_model(model)

tflite_model = converter.convert()

# Save the model.
with open(current_path + '/tf_lite/vegetation_model.tflite', 'wb+') as f:
  f.write(tflite_model)