import numpy as np
import tensorflow as tf
import cv2
import os
import time
from PIL import Image #very useful for image resizing

import tensorflow as tf

class NeuralNetwork():

    def __init__(self):

        # we set the basic parameters of our ne

        self.size = 256

        self.original_size = [0] * 2 #empty array of two positions to store image size 

        # the first thing we will do when loading the neural network interface is to load the model
        self.model_name = 'vegetation_model.tflite'
        self.interpreter = self.load_model()

    

    def load_model(self):
        path = os.getcwd()

        model_path = path + '/tf_lite/' + self.model_name

        print('model path', model_path)

        interpreter = tf.lite.Interpreter(model_path=model_path)
        interpreter.allocate_tensors()

        global input_details, output_details
        input_details = interpreter.get_input_details()
        output_details = interpreter.get_output_details()
        
        # check the type of the input tensor
        floating_model = input_details[0]['dtype'] == np.float32

        # NxHxWxC, H:1, W:2
        height = input_details[0]['shape'][1]
        width = input_details[0]['shape'][2]

        return interpreter

    def resize_image(self, img):    # function to reduce image for model processing
        
        """
        There are two methods for doing that: PIL and OpenCV. OpenCV is much faster than
        """

        self.original_size[0] = img.shape[0]
        self.original_size[1] = img.shape[1]

        resized = cv2.resize(img, (self.size,self.size), interpolation = cv2.INTER_AREA)

        image = np.asarray(resized)


        """ image = Image.fromarray(img, 'BGR')   # we transofrm the original image into a numpy array
        image = image.resize((self.size, self.size))  """

        image_norm = tf.keras.utils.normalize(np.array(image), axis=1, order=2)
        
        image_norm = np.expand_dims(image_norm, 2)
    
        image_input = np.expand_dims(image_norm,0)
        image_input = image_input*10

        return image_input
        # image_file_resized = cv2.resize(image_file,(2304,1792), fx = 0, fy = 0, interpolation = cv2.INTER_NEAREST)

    def predict_image(self, raw_image):
        
        """
        Input image to model.predict function should have this format:

            image to be processed format:  float32
            image to be processed shape:  (1, 256, 256, 1)
            scaled from [0-1], not [0-255]
        """
        image_input = self.resize_image(raw_image).astype(np.float32)

        print('@@@@@@@ image input @@@@@@@@@@@', image_input)

        self.interpreter.set_tensor(input_details[0]['index'], image_input)

        start_time = time.time()
        self.interpreter.invoke()
        stop_time = time.time()
        
        print('Processing time', stop_time-start_time)

        output_data = self.interpreter.get_tensor(output_details[0]['index'])

        print('********* output data from model processing ******', output_data)

        results = np.squeeze(output_data)

        prediction_mask = cv2.resize(results.astype(float),(self.original_size[1],self.original_size[0]), fx = 0, fy = 0, interpolation = cv2.INTER_NEAREST)

        prediction_mask[prediction_mask < 0.15] = 0
        prediction_mask[prediction_mask > 0.1] = 1
        print(' ^^^^^^^ resized output ^^^^^', prediction_mask)

        #prediction_mask = np.asarray(prediction_mask)

        print('"""""""""  final resut """""""""', prediction_mask)

        return prediction_mask

    
    #def recover_image(self, prediction_mask): #function to recover original image sizing after model processing

