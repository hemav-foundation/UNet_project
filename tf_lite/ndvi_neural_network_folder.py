import numpy as np
import os
import json
import pandas as pd
import cv2
import math
from neural_network_interface import NeuralNetwork
import time

def contrast_stretch(im):
    """
    Performs a simple contrast stretch of the given image, from 5-95%.
    """
    in_min = np.percentile(im, 1)
    in_max = np.percentile(im, 100)

    out_min = 0.0
    out_max = 255.0

    out = im - in_min
    out *= ((out_min - out_max) / (in_min - in_max))
    out += in_min

    return out

def main_loop(img, dest_path, image, NN):
    # Once we have the original image, we need to take the red and nir channels to operate with them

    b = np.array(img[:, :, 0]).astype(float) + 0.00000000001
    r = np.array(img[:, :, 2]).astype(float) + 0.00000000001

    NN_mask = NN.predict_image(b)

    height = NN_mask.shape[0]
    width = NN_mask.shape[1]
    #channels = NN_mask.shape[2]

    #print('properties: %s, %s', np.dtype(NN_mask), np.ndim(NN_mask))
   
    print('Image Height       : ', height)
    print('Image Width        : ', width)
    #print('Number of Channels : ', channels)


    # we want to delete shadows from the original image, as they are introducing distorsions
    lower_limit = np.array([3, 3, 3])
    upper_limit = np.array([255, 255, 255])
    shadows = cv2.inRange(img, lower_limit, upper_limit)

    # using the blue filter, red channel is NIR band and blue channel is visible light

    kernel = np.ones((1, 1), np.uint8)
    dilation = cv2.dilate(r, kernel, iterations=2)
    nir = dilation*1.2
    red = b*0.8

    np.seterr(divide='ignore', invalid='ignore')

    # we compute the ndvi
    ndvi = ((nir - red) / (nir + red)).astype(float)

    # once we have the ndvi in (-1, 1) scale, we convert it to 0-255 scale to operate with opencv

    ndvi_contrasted = contrast_stretch(ndvi).astype(np.uint8)

    # we delete the shadows from the ndvi re-scaled image
    ndvi_new = cv2.bitwise_or(ndvi_contrasted, ndvi_contrasted, mask=shadows)

    """ # we apply some morphological operations to enhance vegetation

    kernel = np.ones((1, 1), np.uint8)
    erosion = cv2.erode(ndvi_new, kernel, iterations=1)

    kernel = np.ones((2, 2), np.uint8)
    dilation = cv2.dilate(erosion, kernel, iterations=3)

    ndvi_new = dilation """
    #ndvi_values = np.count_nonzero(ndvi_new > 126)

    # once we have the final image with vegetation, we remove everything that is under 0.14 (163) NDVI value
    ndvi_new[ndvi_new < 163] = 0

    #cv2.imshow('old ndvi', ndvi_new)


    ndvi_new = cv2.bitwise_and(ndvi_new, ndvi_new, mask=NN_mask.astype(np.uint8))
    #cv2.imshow('NNN NDVI', ndvi_new)

    kernel = np.ones((3, 3), np.uint8)
    dilation = cv2.dilate(ndvi_new, kernel, iterations=3)

    ndvi_new = dilation

    ndvi_values = np.count_nonzero(ndvi_new > 0)

    total_values = ndvi_new.shape[0] * ndvi_new.shape[1]

    percent = round(((ndvi_values / total_values) * 100), 2)

    print('Percentage of vegetation', percent)

    if percent >= 0:
        
        #cv2.imshow('image normal', img)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        equ = cv2.equalizeHist(img)
        
        img = cv2.cvtColor(equ, cv2.COLOR_GRAY2BGR)

        name_vegetation = dest_path + image

        # to create the final output, we want to add what is vegetation to the raw image

        mask_vegetation = cv2.inRange(ndvi_new, 163, 255)
        
        res = cv2.bitwise_and(img, img, mask=cv2.bitwise_not(mask_vegetation))
        ndvi_final = cv2.cvtColor(ndvi_new, cv2.COLOR_GRAY2BGR)

        ndvi_colored = cv2.applyColorMap(ndvi_final, cv2.COLORMAP_JET)
        ndvi_final = ndvi_colored

        ndvi_result = cv2.bitwise_and(ndvi_final, ndvi_final, mask=mask_vegetation)

        # fusion is the final output, containing vegetation and original image
        fusion = res + ndvi_result
        #cv2.imshow('final ndvi', fusion)

        cv2.imwrite(name_vegetation, fusion)
        #cv2.waitKey(0)


path = "/Users/XavierBalaguer/Desktop/2021_11_05-07_26_HP2-082/ndvi_images/raw_images/"
dest_path = "/Users/XavierBalaguer/Desktop/2021_11_05-07_26_HP2-082/new_ndvi/"

images = os.listdir(path)

NN = NeuralNetwork()

for image in images:

    if image.split('.')[1] != "png":
        continue

    image2 = cv2.imread(path + image)

    start = time.perf_counter()
    main_loop(image2, dest_path, image, NN)

    total_time = time.perf_counter() - start

    print(' ########  PROCESSING TIME: %s ##########' % total_time)
	